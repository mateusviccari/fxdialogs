package test.br.mviccari.fxdialogs;

import br.mviccari.fxdialogs.dialogs.Dialog;
import br.mviccari.fxdialogs.dialogs.Option;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

public class Tests extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Button button = new Button();
        button.setText("Click!");
        button.setOnAction(event -> {
            Dialog.build(primaryStage).setMessage("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac blandit nisl. Nullam tempus molestie lacus. Cras semper lacus id dapibus interdum. Pellentesque tortor leo, tincidunt ut ipsum et, auctor scelerisque elit. Pellentesque augue ipsum, scelerisque sed nisl eget, ultrices consectetur massa. Nulla tempor magna eget tellus hendrerit, vitae dictum sem adipiscing. Phasellus quis ipsum sed dui eleifend tincidunt. Donec et diam ut nunc pellentesque eleifend quis sit amet nibh. Nullam ac semper augue. In est sem, mollis vel malesuada ac, facilisis nec justo. Pellentesque tincidunt nec tellus non tincidunt. Donec erat orci, hendrerit ac adipiscing vel, iaculis quis arcu. Nam lacus nulla, iaculis vitae consequat id, condimentum sit amet metus. Proin eget libero nec risus iaculis dignissim nec vitae ligula. Sed sit amet tincidunt ipsum, pretium mollis quam.").showMessage();

            Option option = Dialog.build(primaryStage).setOptions(Option.OK, new Option("Foda-se", KeyCode.F), Option.CANCEL).setTitle("Selecione a opção!").showOptions();
            Dialog.build(primaryStage).setTitle("Voce clicou em: " + option + "!").showMessage();
        });
        button.setPrefSize(120,50);
        Scene scene = new Scene(button,120,50);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch();
    }
}
