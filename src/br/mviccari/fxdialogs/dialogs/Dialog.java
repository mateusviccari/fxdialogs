package br.mviccari.fxdialogs.dialogs;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.util.Locale;
import java.util.ResourceBundle;

public class Dialog {

    public Label labelTitle;
    public Label labelMessage;
    public HBox optionsBox;
    public AnchorPane anchorPaneMessage;
    public AnchorPane rootAnchorPane;
    public AnchorPane anchorPaneDialog;

    private Stage stage;
    private Option selectedOption;
    private Double initialX, initialY;
    private String title;
    private String message;
    private ResourceBundle resourceBundle;
    private DialogType dialogType;

    public static Dialog build(Stage parent){
        try {
            FXMLLoader loader = new FXMLLoader(Dialog.class.getResource("/br/mviccari/fxdialogs/layouts/default_dialog.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            scene.setFill(null);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initOwner(parent);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.TRANSPARENT);
            Dialog controller = loader.getController();
            controller.init(stage);
            return controller;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error creating dialog", e);
        }
    }

    private void init(Stage stage){
        this.stage = stage;
        makeWindowDraggable();
        resourceBundle = ResourceBundle.getBundle("FxDialog", Locale.getDefault());
    }

    public Dialog setTitle(String title){
        this.title = title;
        return this;
    }

    public Dialog setMessage(String message){
        this.message = message;
        return this;
    }

    public Dialog setOptions(Option... options){
        optionsBox.getChildren().clear();
        for (Option option : options) {
            Button optionButton = new Button(option.toString());
            optionButton.setOnAction(event -> {
                this.selectedOption = option;
                stage.close();
            });
            HBox.setHgrow(optionButton, Priority.ALWAYS);
            optionButton.setMaxHeight(Double.MAX_VALUE);
            optionButton.setMaxWidth(Double.MAX_VALUE);
            optionButton.getStyleClass().add("rich-blue");
            optionsBox.getChildren().add(optionButton);
        }
        loadMnemonicsForOptions(options);
        return this;
    }

    public void showMessage(){
        dialogType=DialogType.MESSAGE;
        setOptions(Option.OK);
        loadMessage();
        stage.showAndWait();
    }

    public Option showOptions(){
        dialogType=DialogType.OPTIONS;
        if(optionsBox.getChildren().isEmpty()){
            throw new IllegalArgumentException("No options were set!");
        }
        loadMessage();
        stage.showAndWait();
        return selectedOption;
    }

    private void loadMessage() {
        if(title==null){
            if(dialogType.equals(DialogType.MESSAGE)){
                title = resourceBundle.getString("default_message_title");
            }else{
                title = resourceBundle.getString("default_options_title");
            }
        }
        labelTitle.setText(title);
        labelMessage.setText(message);
    }

    public Option getSelectedOption(){
        return selectedOption;
    }

    private void makeWindowDraggable(){
        anchorPaneDialog.setOnMousePressed((MouseEvent me) -> {
            if (me.getButton() != MouseButton.MIDDLE) {
                initialX = me.getSceneX();
                initialY = me.getSceneY();
            }
        });

        anchorPaneDialog.setOnMouseDragged((MouseEvent me) -> {
            if (me.getButton() != MouseButton.MIDDLE) {
                stage.setX(me.getScreenX() - initialX);
                stage.setY(me.getScreenY() - initialY);
            }
        });
    }

    private void loadMnemonicsForOptions(Option... options) {
        stage.addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            for (Option option : options) {
                KeyCode mnemonic = option.getMnemonic();
                if(option.isDefaultOption()){
                    mnemonic = getMnemonicForOption(option);
                }
                if(mnemonic!=null && mnemonic.equals(event.getCode())){
                    this.selectedOption = option;
                    stage.close();
                }
            }
            event.consume();
        });
    }

    private KeyCode getMnemonicForOption(Option option) {
        String optionMnemonicString = null;
        if(option.equals(Option.OK)){
            optionMnemonicString = resourceBundle.getString("button_ok_mnemonic");
        }else if(option.equals(Option.YES)){
            optionMnemonicString = resourceBundle.getString("button_yes_mnemonic");
        }else if(option.equals(Option.NO)){
            optionMnemonicString = resourceBundle.getString("button_no_mnemonic");
        }else if(option.equals(Option.CANCEL)){
            optionMnemonicString = resourceBundle.getString("button_cancel_mnemonic");
        }
        if(optionMnemonicString==null){
            return null;
        }else{
            try{
                return KeyCode.getKeyCode(optionMnemonicString.toUpperCase());
            }catch (Exception ex){
                throw new RuntimeException("Error loading mnemonic for option \""+option+"\".", ex);
            }
        }
    }

}
