package br.mviccari.fxdialogs.dialogs;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;

public class Option {
    private Object value;
    private KeyCode mnemonic;
    private boolean defaultOption = false;

    public static final Option OK = new Option("OK", true);
    public static final Option YES = new Option("Yes", true);
    public static final Option NO = new Option("No", true);
    public static final Option CANCEL = new Option("Cancel", true);

    public Option(Object value) {
        this.value = value;
    }

    private Option(Object value, boolean defaultOption) {
        this.value = value;
        this.defaultOption = defaultOption;
    }

    public Option(Object value, KeyCode mnemonic) {
        this.value = value;
        this.mnemonic = mnemonic;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public KeyCode getMnemonic() {
        return mnemonic;
    }

    public void setMnemonic(KeyCode mnemonic) {
        this.mnemonic = mnemonic;
    }

    /**
     * Returns true if this is an internal default option, e.g YES, NO, CANCEL, OK...
     */
    public boolean isDefaultOption() {
        return defaultOption;
    }
}
